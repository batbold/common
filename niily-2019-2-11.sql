--
-- PostgreSQL database dump
--

-- Dumped from database version 11.0
-- Dumped by pg_dump version 11.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Order; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Order" (
    id integer NOT NULL,
    city character varying,
    district character varying,
    "subDistrict" character varying,
    address character varying,
    phone character varying,
    "userPhoneId" integer NOT NULL,
    name character varying,
    other character varying,
    "PSID" character varying NOT NULL,
    "ASID" character varying NOT NULL,
    "clientType" integer NOT NULL,
    status integer NOT NULL,
    paid boolean NOT NULL,
    "paymentDesc" character varying NOT NULL,
    amount integer NOT NULL,
    "bankOrderId" character varying NOT NULL,
    bank character varying NOT NULL,
    canceled boolean NOT NULL,
    "productId" integer NOT NULL,
    quantity integer NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL
);


--
-- Name: Order_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Order_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Order_id_seq" OWNED BY public."Order".id;


--
-- Name: Product; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Product" (
    id integer NOT NULL,
    category integer[] NOT NULL,
    image1 character varying NOT NULL,
    image2 character varying,
    images character varying[] NOT NULL,
    name character varying NOT NULL,
    specialty character varying NOT NULL,
    stock integer NOT NULL,
    "finishDatetime" timestamp without time zone NOT NULL,
    status integer NOT NULL,
    gradual jsonb,
    custom jsonb,
    supplier character varying NOT NULL,
    "desc" character varying NOT NULL,
    "orderWeight" integer NOT NULL,
    barcode character varying NOT NULL,
    "searchWords" character varying NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL
);


--
-- Name: Product_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Product_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: Product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Product_id_seq" OWNED BY public."Product".id;


--
-- Name: UserPhone; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."UserPhone" (
    id integer NOT NULL,
    phone character varying NOT NULL,
    "PSID" character varying NOT NULL,
    secret character varying NOT NULL,
    verified boolean NOT NULL,
    "blackListed" boolean NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL
);


--
-- Name: UserPhone_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."UserPhone_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: UserPhone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."UserPhone_id_seq" OWNED BY public."UserPhone".id;


--
-- Name: Order id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Order" ALTER COLUMN id SET DEFAULT nextval('public."Order_id_seq"'::regclass);


--
-- Name: Product id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Product" ALTER COLUMN id SET DEFAULT nextval('public."Product_id_seq"'::regclass);


--
-- Name: UserPhone id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserPhone" ALTER COLUMN id SET DEFAULT nextval('public."UserPhone_id_seq"'::regclass);


--
-- Name: Order Order_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_pkey" PRIMARY KEY (id);


--
-- Name: Product Product_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Product"
    ADD CONSTRAINT "Product_pkey" PRIMARY KEY (id);


--
-- Name: UserPhone UserPhone_phone_PSID_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserPhone"
    ADD CONSTRAINT "UserPhone_phone_PSID_key" UNIQUE (phone, "PSID");


--
-- Name: UserPhone UserPhone_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserPhone"
    ADD CONSTRAINT "UserPhone_pkey" PRIMARY KEY (id);


--
-- Name: Order_ASID_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_ASID_index" ON public."Order" USING hash ("ASID");


--
-- Name: Order_PSID_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_PSID_index" ON public."Order" USING hash ("PSID");


--
-- Name: Order_bankOrderId_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_bankOrderId_index" ON public."Order" USING hash (lower(("bankOrderId")::text));


--
-- Name: Order_bank_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_bank_index" ON public."Order" USING hash (lower((bank)::text));


--
-- Name: Order_city_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_city_index" ON public."Order" USING hash (lower((city)::text));


--
-- Name: Order_district_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_district_index" ON public."Order" USING hash (lower((district)::text));


--
-- Name: Order_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_name_index" ON public."Order" USING hash (lower((name)::text));


--
-- Name: Order_phone_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_phone_index" ON public."Order" USING hash (lower((phone)::text));


--
-- Name: Order_quantity_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_quantity_index" ON public."Order" USING btree (quantity);


--
-- Name: Order_subDistrict_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Order_subDistrict_index" ON public."Order" USING hash (lower(("subDistrict")::text));


--
-- Name: Product_barcode; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Product_barcode" ON public."Product" USING hash (barcode);


--
-- Name: Product_category; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Product_category" ON public."Product" USING gin (category);


--
-- Name: Product_search_index_pg_trgm; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Product_search_index_pg_trgm" ON public."Product" USING gin (lower((((((((name)::text || ' '::text) || (specialty)::text) || ' '::text) || ("searchWords")::text) || ' '::text) || (barcode)::text)) public.gin_trgm_ops);


--
-- Name: Product_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "Product_status" ON public."Product" USING hash (status);


--
-- Name: UserPhone_PSID; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "UserPhone_PSID" ON public."UserPhone" USING hash ("PSID");


--
-- Name: UserPhone_phone; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "UserPhone_phone" ON public."UserPhone" USING hash (phone);


--
-- Name: Order Order_productId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_productId_fkey" FOREIGN KEY ("productId") REFERENCES public."Product"(id);


--
-- Name: Order Order_userPhoneId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Order"
    ADD CONSTRAINT "Order_userPhoneId_fkey" FOREIGN KEY ("userPhoneId") REFERENCES public."UserPhone"(id);


--
-- PostgreSQL database dump complete
--

